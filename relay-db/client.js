'use strict';
var redis = require('redis');

module.exports = function client(options){
  return redis.createClient();
};
