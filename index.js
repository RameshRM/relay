'use strict';
require('./task-runner');

var express = require('express');

var app = express();
var _port = process.env.PORT || 3000;
var exphbs = require('express3-handlebars');
var bodyParser = require('body-parser');
console.time('AppStart');
app.listen(_port, function listen() {
  console.timeEnd('AppStart');
});

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
  extended: true
}));
app.engine('hbs', exphbs({
  extname: 'hbs'
}));
app.set('view engine', 'hbs');
app.use(require('meta-router/middleware').match(require.resolve('./routes.json')));
app.use(require('meta-router/middleware').invokeHandler());
