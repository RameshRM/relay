'use strict';
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  host: 'email-smtp.us-west-2.amazonaws.com',
  port: 465,
  connectionTimeout: 2000,
  socketTimeout: 1000,
  greetingsTimeout: 1000,
  secure: true
  
});
var _from = 'rmrmail@gmail.com';

module.exports.mailer = {
  send: function (mailTo, subject, body, options, callback) {
    transporter.sendMail({
      from: _from,
      to: mailTo,
      subject: subject,
      text: body,
      html: options.html
    }, callback);
  }

};
