'use strict';
var dbClient = require('../relay-db').getClient();
var mailer = require('../mailer').mailer;
var composer = require('../mailer/compose');
var util = require('util');
var uuid = require('node-uuid');

function findById(id, callback) {
  dbClient.get(id, function (err, result) {
    callback(err, JSON.parse(result));
  });

}

function Relay(options) {
  options = options || {};
  this.id = uuid.v4();
  this.name = options.name;
  delete options.name;
  this.attrs = options;
}

Relay.prototype.store = function (options, callback) {

  dbClient.set(this.id, JSON.stringify(this.attrs));
  this.attrs.id = this.id;
  Relay.pass(this.attrs, callback);

};

Relay.prototype.store = function (options, callback) {
  dbClient.set(this.id, JSON.stringify(this.attrs));
  this.attrs.id = this.id;
  Relay.pass(this.attrs, callback);
};

Relay.pass = function (options, callback) {
  var uriBase = process.env.baseURL || 'localhost:3000';
  console.log(uriBase);
  var composed = composer({
    receiver: options.receiver || 'Unknown',
    sender: 'rmrmail@gmail.com',
    name: options._file.name,
    link: encodeURI(util.format('http://%s/fetch?ds=%s&token=%s&name=', uriBase, Date.now(), options.id, options._file.name))
  });

  process.emit('notify', {
    receiver: options.receiver,
    subject: 'Testing Project Relay',
    text: 'New File Shared',
    html: composed
  });

};

Relay.isUsed = function (id, callback) {
  dbClient.get(id, function (err, result) {
    callback(err, result);
  });
};

Relay.find = function find(id, filter, callback) {
  if (filter && filter.isActive) {
    Relay.isUsed(id + ':used', function (err, isUsed) {

      if (isUsed) {
        return callback(undefined, undefined);
      } else {
        return findById(id, callback);
      }
    });
  } else {
    return findById(id, callback);
  }
};

Relay.once = function once(id, callback) {
  Relay.find(id, {
    isActive: true
  }, function (err, result) {
    dbClient.set(id + ':used', true);
    if (result && !err) {
      setImmediate(function () {
        process.emit('scavenger', {
          path: result && result._file && result._file.path
        });
      });

    }
    callback(err, result);
  });
};

module.exports.Relay = Relay;
