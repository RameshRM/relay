'use strict';
var FS = require('fs');


process.on('scavenger', runScavenger);

function runScavenger(dataArgs) {
  if (dataArgs && dataArgs.path) {
    FS.unlink(dataArgs.path, function (err) {
    });
  }
}

module.exports = runScavenger;
