'use strict';
var mailer = require('../../mailer').mailer;

function notify(dataArgs) {
  mailer.send(dataArgs.receiver, dataArgs.subject, dataArgs.text, {
    html: dataArgs.html
  });
}

process.on('notify', notify);

module.exports = notify;
