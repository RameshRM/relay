'use strict';
var Relay = require('../relay-models').Relay;
var FS = require('fs');
module.exports = function (req, res) {

  Relay.once(req.query.token, function (err, result) {
    var _file = result && result._file;

    if (_file) {
      res.setHeader('Content-disposition', 'attachment; filename=' + _file.name);
      res.setHeader('Content-type', _file.type);
      FS.createReadStream(_file.path).pipe(res);
    } else {
      res.status(204).send({});
    }
  });

};
