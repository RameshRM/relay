'use strict';
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var Relay = require('../relay-models').Relay;

module.exports = function (req, res) {
  var form = new formidable.IncomingForm();
  var _file;
  var options = {};
  form.uploadDir = path.join(__dirname, '../uploads');
  var fileName;
  var fileArgs = {};

  form.on('fileBegin', function (name, file) {
    fileArgs.name = file.name;
    fileArgs.type = file.type;
    fileArgs.size = file.size;
    fileArgs.path = file.path;

  });
  // form.on('part', function (name, part) {
  //   console.log(name, file.type);;
  // });
  // form.on('file', function (name, file) {});
  //
  // // form.onPart = function (part) {
  // //   part.addListener('data', function (data) {
  // //     if (part.name !== 'file') {
  // //       options[part.name] = data.toString();
  // //     }
  // //
  // //   });
  // // };
  form.parse(req, function (err, fields, files) {
    if (fields) {
      options.receiver = fields.receiver;
      options.notes = fields.notes;
    }
    if (files) {
      options._file = fileArgs;
    }
  });

  form.on('end', function end() {
    res.send(options);
    var _relay = new Relay(options);
    _relay.store({}, function (err, result) {
      res.send(result);
    });
  });

};
